﻿using pTranslations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SpoofControl.ChangedAntennaDirect += OnChangedAntennaDirect;
            SpoofControl.ChangedNavigation += SpoofControl_ChangedNavigation;
        }

        private void OnChangedAntennaDirect(object sender, int e)
        {
            int direct = e;
        }

        private void SpoofControl_ChangedNavigation(object sender, SpoofingControl.NavigationModel e)
        {
            Console.WriteLine(e);
        }

        private void b1_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl.Orientation = SpoofingControl.OrientationEnum.Vertical;
        }

        private void b2_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl.Orientation = SpoofingControl.OrientationEnum.Horizontal;
        }

        private void b3_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl.State1 = new SpoofingControl.StateModel1() { Latitude = 100 };
            SpoofControl.State2 = new SpoofingControl.StateModel2() { Temperature = 100 };
            SpoofControl.ImageAngle = new SpoofingControl.ImageModel() { Angle = 100 };
            var te = SpoofControl.Antenna;
            var tem = SpoofControl.SpoofParameters;
        }

        private void b4_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl.CurrentTheme = (SpoofControl.CurrentTheme == SpoofingControl.Theme.Orange) ? SpoofingControl.Theme.Blue : SpoofingControl.Theme.Orange;
        }

        private void b5_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl.ChangeLanguage(Languages.RU);
        }

        private void b6_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl.ChangeLanguage(Languages.EN);
        }

        private void b7_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl.ChangeLanguage(Languages.AZ);
        }

        private void b8_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl.View = SpoofingControl.View.Normal;
        }

        private void b9_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl.View = SpoofingControl.View.Pro;
        }
    }
}
