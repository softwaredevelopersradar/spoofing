﻿using pTranslations;
using StateNAVControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SpoofControl2.ChangedAntennaType += SpoofControl2_ChangedAntennaType;
            SpoofControl2.ChangedAntennaDirect += OnChangedAntennaDirect;
            SpoofControl2.ChangedNavigation += SpoofControl2_ChangedNavigation;
        }

        private void SpoofControl2_ChangedAntennaType(object sender, SpoofingControl.AntennaMode e)
        {
            Console.WriteLine(e);
        }

        private void OnChangedAntennaDirect(object sender, int e)
        {
            int direct = e;
        }

        private void SpoofControl2_ChangedNavigation(object sender, SpoofingControl.NavigationModel e)
        {
            Console.WriteLine(e);
        }

        private void b1_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.Orientation = SpoofingControl.OrientationEnum.Vertical;
        }

        private void b2_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.Orientation = SpoofingControl.OrientationEnum.Horizontal;
        }

        private void b3_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.State1 = new SpoofingControl.StateModel1() { Latitude = 100 };
            SpoofControl2.State2 = new SpoofingControl.StateModel2() { Temperature = 100 };
            SpoofControl2.ImageAngle = new SpoofingControl.ImageModel() { Angle = 100 };
            var te = SpoofControl2.Antenna;
            var tem = SpoofControl2.SpoofParameters;
        }

        private void b4_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.CurrentTheme = (SpoofControl2.CurrentTheme == SpoofingControl.Theme.Orange) ? SpoofingControl.Theme.Blue : SpoofingControl.Theme.Orange;
        }

        private void b5_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.ChangeLanguage(Languages.RU);
        }

        private void b6_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.ChangeLanguage(Languages.EN);
        }

        private void b7_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.ChangeLanguage(Languages.AZ);
        }

        private void b8_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.View = SpoofingControl.View.Normal;
        }

        private void b9_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.View = SpoofingControl.View.Pro;
        }

        private void b10_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();

            List<StateModel> list = new List<StateModel>()
            {
                new StateModel
                {
                    Id = -2,
                    Type = StateNAVControl.Type.NAV,
                    Temperature = (short)rand.Next(1, 100),
                    Amperage = (float)(rand.Next(1, 100) + rand.NextDouble()),
                    Snt = StateNAVControl.Led.Red,
                    Rad = StateNAVControl.Led.Green,
                    Pow = StateNAVControl.Led.Green
                },
                new StateModel
                {
                    Id = -2,
                    Type = StateNAVControl.Type.SPUF,
                    Temperature = -102,
                    Amperage = -2,
                    Snt = StateNAVControl.Led.Gray,
                    Rad = StateNAVControl.Led.Green,
                    Pow = StateNAVControl.Led.Red
                }
            };

            SpoofControl2.SetBPSSParams(list);
        }

        private void b11_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.SetAntennaMode(SpoofingControl.AntennaMode.OMNI);
        }

        private void b12_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            SpoofControl2.State1 = new SpoofingControl.StateModel1()
            {
                Latitude = 53 + random.NextDouble(),
                Longitude = 27 + random.NextDouble(),
                Time = DateTime.Now
            };
        }

        private void b13_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.NavigationToggleButtonOff();
        }

        private void b14_Click(object sender, RoutedEventArgs e)
        {
            SpoofControl2.SpoofingToggleButtonOff();

        }
    }
}
