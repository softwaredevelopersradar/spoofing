﻿using System.Windows.Input;
using System.Windows.Data;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using System.Collections.Generic;
using System;
using Xceed.Wpf.Toolkit;

namespace SpoofingControl
{
    public static class UpDownEditor
    {
        #region OnTyping Int

        public static readonly DependencyProperty CommitOnIntTypingProperty = DependencyProperty.RegisterAttached("CommitOnIntTyping", typeof(bool), typeof(UpDownEditor), new FrameworkPropertyMetadata(false, OnCommitOnIntTypingChanged));

        private static void OnCommitOnIntTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var integerUpDown = sender as IntegerUpDown;
            if (integerUpDown == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                integerUpDown.KeyUp -= IntegerUpDownCommitIntValueWhileTyping;
                integerUpDown.PreviewTextInput -= IntegerUpDownCommitPreviewTextInput;
                integerUpDown.PreviewKeyDown -= IntegerUpDown_PreviewKeyDown;
            }

            if (needToBind)
            {

                integerUpDown.PreviewTextInput += IntegerUpDownCommitPreviewTextInput;
                integerUpDown.KeyUp += IntegerUpDownCommitIntValueWhileTyping;
                integerUpDown.PreviewKeyDown += IntegerUpDown_PreviewKeyDown;
            }
        }


        private static void IntegerUpDownCommitPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        static void IntegerUpDownCommitIntValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus || e.Key == Key.Escape)
                return;

            var integerUpDown = sender as IntegerUpDown;

            if (integerUpDown == null) return;

            if (integerUpDown.Text == "") return;

            BindingExpression expression = integerUpDown.GetBindingExpression(IntegerUpDown.TextProperty);
            if (expression != null) expression.UpdateSource();
            e.Handled = true;
        }

        public static void SetCommitOnIntTyping(IntegerUpDown target, bool value)
        {
            target.SetValue(CommitOnIntTypingProperty, value);
        }

        public static bool GetCommitOnIntTyping(IntegerUpDown target)
        {
            return (bool)target.GetValue(CommitOnIntTypingProperty);
        }


        private static void IntegerUpDown_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                var integerUpDown = sender as IntegerUpDown;
                var parent = integerUpDown.Parent;
                foreach (var final in (parent as Grid).Children)
                {
                    if (final.Equals(sender)) continue;

                    if (!(final is IntegerUpDown)) continue;
                    if ((final as IntegerUpDown).IsEnabled && (final as IntegerUpDown).Focusable && !(final as IntegerUpDown).IsReadOnly)
                    {
                        (final as UIElement).Focus();
                        e.Handled = true;
                        return;
                    }
                }
                (sender as UIElement).Focus();
                e.Handled = true;
            }

        }
        #endregion
    }
}
