﻿using pTranslations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SpoofingControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class CoordinatesControl : UserControl, INotifyPropertyChanged
    {
        internal event EventHandler<CoordinatesModel> ChangedCoordinates;

        private Theme _CurrentTheme;
        public Theme CurrentTheme
        {
            get { return _CurrentTheme; }
            set
            {
                _CurrentTheme = value;
                SetTheme(value);
            }
        }
        private void SetTheme(Theme CurrentTheme)
        {
            ThemeSwither.SetTheme(CurrentTheme, this);
        }


        private View _View = View.Normal;
        public View View
        {
            get { return _View; }
            set
            {
                _View = value;
                OnPropertyChanged(nameof(View));
            }
        }

        private bool _SpoofButtonVisible = false;
        public bool SpoofButtonVisible
        {
            get { return _SpoofButtonVisible; }
            set
            {
                _SpoofButtonVisible = value;
                OnPropertyChanged(nameof(SpoofButtonVisible));
            }
        }

        private bool _IsEnabledButtons = true;
        public bool IsEnabledButtons
        {
            get { return _IsEnabledButtons; }
            set
            {
                _IsEnabledButtons = value;
                OnPropertyChanged(nameof(IsEnabledButtons));
            }
        }


        public readonly Translations translations;

        public CoordinatesControl()
        {
            InitializeComponent();
            cmbxGateList.ItemsSource = rangeGateList;
            cmbxGateList.SelectedIndex = 1;

            translations = new Translations(Properties.Resources.TranslationCoordinates, MainGrid.Children);
        }


        internal static List<string> rangeGateList = new List<string> { " < 1 km", " 1..2 km", " 2..5 km", " 5..10 km", " 10..20 km", " > 20 km"};


        public static readonly DependencyProperty CoordinatesProperty = DependencyProperty.Register("Coordinates", typeof(CoordinatesModel), typeof(CoordinatesControl),
            new FrameworkPropertyMetadata(new CoordinatesModel { Latitude = 0, Altitude = 0, Longitude = 0, Heading = 0, RangeGate = 1, Speed = 0 }, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public CoordinatesModel Coordinates
        {
            get { return (CoordinatesModel)GetValue(CoordinatesProperty); }
            set { SetValue(CoordinatesProperty, value); }
        }

        private void Coords_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ChangedCoordinates?.Invoke(this, Coordinates);
        }

        private void cmbxGateList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChangedCoordinates?.Invoke(this, Coordinates);
        }

        public delegate void ToggleButtonCheked(bool isCheked, byte powerLevel);
        public event ToggleButtonCheked OnSpoofClick;

        private void tbSpoof_Click(object sender, RoutedEventArgs e)
        {
            OnSpoofClick?.Invoke(tbSpoof.IsChecked.Value, (byte)powerLevel.Value);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private void latlon_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                ChangedCoordinates?.Invoke(this, Coordinates);
            }
        }
    }

}


