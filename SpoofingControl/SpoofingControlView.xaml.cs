﻿using System;
using System.Windows;
using System.Windows.Controls;
using pTranslations;

namespace SpoofingControl
{
    /// <summary>
    /// Interaction logic for SpoofingControlView.xaml
    /// </summary>
    public partial class SpoofingControlView : UserControl
    {
        SpoofingViewModel spoofingViewModel = new SpoofingViewModel();

        public event EventHandler<int> ChangedAntennaDirect;
        public event EventHandler<AntennaMode> ChangedAntennaType;
        public event EventHandler<CoordinatesModel> ChangedCoordinates;
        public event EventHandler<NavigationModel> ChangedNavigation;
        public event EventHandler<MainToggleButtonsModel> ChangedMainToggleButtons;


        private View _View = View.Normal;
        public View View
        {
            get { return _View; }
            set
            {
                _View = value;
                nControl.View = value;
                cControl.View = value;
            }
        }

        public SpoofingControlView()
        {
            InitializeComponent();
            this.DataContext = spoofingViewModel;
            aControl.ChangedAntennaDirect += AControl_ChangedAntennaDirect;
            aControl.ChangedAntennaType += AControl_ChangedAntennaType;
            cControl.ChangedCoordinates += CControl_ChangedCoordinates;
            nControl.OnNaviBtnsClick += NControl_OnNaviBtnsClick;

            cControl.OnSpoofClick += CControl_OnSpoofClick;
            nControl.OnNaviJamClick += NControl_OnNaviJamClick;

             CurrentTheme = Theme.Blue;
        }

        private void CControl_ChangedCoordinates(object sender, CoordinatesModel e)
        {
            iControl.ImageValue.Angle = e.Heading;
            ChangedCoordinates?.Invoke(this, e);
        }

        private void AControl_ChangedAntennaType(object sender, AntennaMode e)
        {
            ChangedAntennaType?.Invoke(this, e);
        }

        private void AControl_ChangedAntennaDirect(object sender, int e)
        {
            ChangedAntennaDirect?.Invoke(this, e);
        }

        private void NControl_OnNaviBtnsClick(bool isCheked, Navi navi, L l)
        {
            ChangedNavigation?.Invoke(this, new NavigationModel() { IsCheked = isCheked, Navi = navi, L = l });
        }

        private void CControl_OnSpoofClick(bool isCheked, byte powerlevel)
        {
            ChangedMainToggleButtons?.Invoke(this, new MainToggleButtonsModel() { IsCheked = isCheked, SpoofAndNavi = SpoofOrNavi.Spoofing, PowerLevel = powerlevel });
        }

        private void NControl_OnNaviJamClick(bool isCheked, byte powerlevel)
        {
            ChangedMainToggleButtons?.Invoke(this, new MainToggleButtonsModel() { IsCheked = isCheked, SpoofAndNavi = SpoofOrNavi.NaviJamming, PowerLevel = powerlevel });
        }

        public AntennaModel Antenna
        {
            get => spoofingViewModel.TempAntenna;
        }

        public CoordinatesModel SpoofParameters
        {
            get => spoofingViewModel.TempCoordinates;
        }


        public StateModel1 State1
        {
            get => spoofingViewModel.TempState1;
            set
            {
                if (spoofingViewModel.TempState1 == value) return;
                spoofingViewModel.TempState1 = value;
            }
        }

        public StateModel2 State2
        {
            get => spoofingViewModel.TempState2;
            set
            {
                if (spoofingViewModel.TempState2 == value) return;
                spoofingViewModel.TempState2 = value;
            }
        }


        public ImageModel ImageAngle
        {
            get => spoofingViewModel.TempImage;
            set
            {
                if (spoofingViewModel.TempImage == value) return;
                spoofingViewModel.TempImage = value;
            }
        }


        public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register("Orientation", typeof(OrientationEnum), typeof(SpoofingControlView),
       new FrameworkPropertyMetadata(OrientationEnum.Horizontal, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public OrientationEnum Orientation
        {
            get { return (OrientationEnum)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public void ChangeLanguage(Languages language)
        {
            cControl.translations.CurrentLanguage = language;
            aControl.translations.CurrentLanguage = language;
            sControl.translations.CurrentLanguage = language;
            iControl.translations.CurrentLanguage = language;
            nControl.translations.CurrentLanguage = language;
        }

        private Theme _CurrentTheme;
        public Theme CurrentTheme
        {
            get { return _CurrentTheme; }
            set
            {
                _CurrentTheme = value;
                SetTheme(value);
            }
        }

        private void SetTheme(Theme CurrentTheme)
        {
            cControl.CurrentTheme = CurrentTheme;
            aControl.CurrentTheme = CurrentTheme;
            sControl.CurrentTheme = CurrentTheme;
            iControl.CurrentTheme = CurrentTheme;
            nControl.CurrentTheme = CurrentTheme;
        }
    }
}
