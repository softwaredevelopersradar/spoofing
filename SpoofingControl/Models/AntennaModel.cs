﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SpoofingControl
{
    public class AntennaModel : INotifyPropertyChanged
    {
        private int _direct = 5;
        public int Direct
        {
            get => _direct; 
            set
            {
                if (_direct == value) return;
                
                _direct = value;
                OnPropertyChanged();
            }
        }


        private AntennaMode _mode;
        public AntennaMode Mode
        {
            get => _mode;
            set
            {
                if (_mode == value) return;
                _mode = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
