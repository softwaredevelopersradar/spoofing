﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SpoofingControl
{
    public class ImageModel : INotifyPropertyChanged
    {
        private double _angle;
        public double Angle
        {
            get => _angle;
            set
            {
                if (_angle == value) return;

                _angle = value;
                OnPropertyChanged();
            }
        }
        

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
