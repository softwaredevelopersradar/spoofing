﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpoofingControl
{
    public class MainToggleButtonsModel
    {
            public bool IsCheked { get; set; }
            public SpoofOrNavi SpoofAndNavi { get; set; }
            public byte PowerLevel { get; set; }
    }
}
