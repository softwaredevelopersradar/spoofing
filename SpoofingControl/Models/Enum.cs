﻿
namespace SpoofingControl
{
    public enum OrientationEnum
    {
        Horizontal,
        Vertical
    }

    public enum AntennaMode
    {
        Log,
        OMNI
    }

    public enum Navi
    {
        GPS,
        GLONASS
    }

    public enum L
    {
        L1,
        L2
    }

    public enum SpoofOrNavi
    { 
        Spoofing,
        NaviJamming
    }

    public enum View
    {
        Normal,
        Pro
    }
}
