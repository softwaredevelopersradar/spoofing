﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SpoofingControl
{
    public class CoordinatesModel : INotifyPropertyChanged
    {
        private double _latitude;
        public double Latitude
        {
            get => _latitude;
            set
            {
                if (_latitude == value) return;

                _latitude = value;
                OnPropertyChanged();
            }
        }


        private double _longitude;
        public double Longitude
        {
            get => _longitude;
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }


        private short _altitude;
        public short Altitude
        {
            get => _altitude;
            set
            {
                if (_altitude == value) return;
                _altitude = value;
                OnPropertyChanged();
            }
        }


        private float _speed;
        public float Speed
        {
            get => _speed;
            set
            {
                if (_speed == value) return;
                _speed = value;
                OnPropertyChanged();
            }
        }


        private short _heading;
        public short Heading
        {
            get => _heading;
            set
            {
                if (_heading == value) return;
                _heading = value;
                OnPropertyChanged();
            }
        }


        private byte _rangeGate;
        public byte RangeGate
        {
            get => _rangeGate;
            set
            {
                if (_rangeGate == value) return;
                _rangeGate = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
