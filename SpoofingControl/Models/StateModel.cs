﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SpoofingControl
{
    public class StateModel1 : INotifyPropertyChanged
    {
        private double _latitude;
        public double Latitude
        {
            get => _latitude;
            set
            {
                if (_latitude == value) return;

                _latitude = value;
                OnPropertyChanged();
            }
        }


        private double _longitude;
        public double Longitude
        {
            get => _longitude;
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }


        private DateTime _time;
        public DateTime Time
        {
            get => _time;
            set
            {
                if (_time == value) return;
                _time = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }

    public class StateModel2 : INotifyPropertyChanged
    {
        private float _temperature;
        public float Temperature
        {
            get => _temperature;
            set
            {
                if (_temperature == value) return;
                _temperature = value;
                OnPropertyChanged();
            }
        }


        private float _current;
        public float Current
        {
            get => _current;
            set
            {
                if (_current == value) return;
                _current = value;
                OnPropertyChanged();
            }
        }


        private byte _snt;
        public byte Snt
        {
            get => _snt;
            set
            {
                if (_snt == value) return;
                _snt = value;
                OnPropertyChanged();
            }
        }


        private byte _rad;
        public byte Rad
        {
            get => _rad;
            set
            {
                if (_rad == value) return;
                _rad = value;
                OnPropertyChanged();
            }
        }


        private byte _pow;
        public byte Pow
        {
            get => _pow;
            set
            {
                if (_pow == value) return;
                _pow = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
