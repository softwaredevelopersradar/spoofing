﻿using pTranslations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SpoofingControl
{
    /// <summary>
    /// Interaction logic for NavigationControl.xaml
    /// </summary>
    public partial class NavigationControl : UserControl, INotifyPropertyChanged
    {

        private Theme _CurrentTheme;
        public Theme CurrentTheme
        {
            get { return _CurrentTheme; }
            set
            {
                _CurrentTheme = value;
                SetTheme(value);
            }
        }
        private void SetTheme(Theme CurrentTheme)
        {
            ThemeSwither.SetTheme(CurrentTheme, this);
        }


        private View _View = View.Normal;
        public View View
        {
            get { return _View; }
            set
            {
                _View = value;
                OnPropertyChanged(nameof(View));
            }
        }

        public readonly Translations translations;

        public NavigationControl()
        {
            InitializeComponent();

            translations = new Translations(Properties.Resources.TranslationNavigation, MainGrid.Children);
        }

        public delegate void OnBtnClickEventHandler(bool isCheked, Navi navi, L l);
        public event OnBtnClickEventHandler OnNaviBtnsClick;

        private void btnGPSL1_Clicked(object sender, RoutedEventArgs e)
        {
            OnNaviBtnsClick?.Invoke(btnGPSL1.IsChecked.Value, Navi.GPS, L.L1);
        }

        private void btnGPSL2_Clicked(object sender, RoutedEventArgs e)
        {
            OnNaviBtnsClick?.Invoke(btnGPSL2.IsChecked.Value, Navi.GPS, L.L2);
        }

        private void btnGLONASSL1_Clicked(object sender, RoutedEventArgs e)
        {
            OnNaviBtnsClick?.Invoke(btnGLONASSL1.IsChecked.Value, Navi.GLONASS, L.L1);
        }

        private void btnGLONASSL2_Clicked(object sender, RoutedEventArgs e)
        {
            OnNaviBtnsClick?.Invoke(btnGLONASSL2.IsChecked.Value, Navi.GLONASS, L.L2);
        }

        public delegate void ToggleButtonCheked(bool isCheked, byte powerlevel);
        public event ToggleButtonCheked OnNaviJamClick;

        private void tbNaviJam_Click(object sender, RoutedEventArgs e)
        {
            OnNaviJamClick?.Invoke(tbNaviJam.IsChecked.Value, (byte)powerLevel.Value);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
