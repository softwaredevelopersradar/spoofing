﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SpoofingControl
{
    public static class ThemeSwither
    {
        public static void SetTheme<T>(Theme CurrentTheme, T userControl, string path = "/SpoofingControl;component/Themes/") where T : UserControl
        {
            userControl.Resources.MergedDictionaries.Clear();

            ResourceDictionary src1 = new ResourceDictionary();
            ResourceDictionary src2 = new ResourceDictionary();
            ResourceDictionary src3 = new ResourceDictionary();

            if (CurrentTheme == Theme.Orange)
            {
                src1.Source = new Uri(path + "Core.xaml", UriKind.RelativeOrAbsolute);
                src2.Source = new Uri(path + "Brushes.xaml", UriKind.RelativeOrAbsolute);
                src3.Source = new Uri(path + "Icons.xaml", UriKind.RelativeOrAbsolute);
            }
            if (CurrentTheme == Theme.Blue)
            {
                src1.Source = new Uri(path + "ThemeBlue.xaml", UriKind.RelativeOrAbsolute);
                src2.Source = new Uri(path + "BrushesBlue.xaml", UriKind.RelativeOrAbsolute);
                src3.Source = new Uri(path + "Icons.xaml", UriKind.RelativeOrAbsolute);
            }

            userControl.Resources.MergedDictionaries.Add(src1);
            userControl.Resources.MergedDictionaries.Add(src2);
            userControl.Resources.MergedDictionaries.Add(src3);
        }
    }

    public enum Theme
    {
        Blue,
        Orange
    }


}
