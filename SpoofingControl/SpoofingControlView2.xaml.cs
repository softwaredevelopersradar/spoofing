﻿using pTranslations;
using StateNAVControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SpoofingControl
{
    /// <summary>
    /// Interaction logic for SpoofingControlView2.xaml
    /// </summary>
    public partial class SpoofingControlView2 : UserControl
    {
        SpoofingViewModel spoofingViewModel = new SpoofingViewModel();

        public event EventHandler<int> ChangedAntennaDirect;
        public event EventHandler<AntennaMode> ChangedAntennaType;
        public event EventHandler<CoordinatesModel> ChangedCoordinates;
        public event EventHandler<NavigationModel> ChangedNavigation;
        public event EventHandler<MainToggleButtonsModel> ChangedMainToggleButtons;


        private View _View = View.Normal;
        public View View
        {
            get { return _View; }
            set
            {
                _View = value;
                nControl2.View = value;
                cControl.View = value;
            }
        }

        public readonly Translations translations;

        public SpoofingControlView2()
        {
            InitializeComponent();
            this.DataContext = spoofingViewModel;
            nControl2.ChangedAntennaDirect += NControl2_ChangedAntennaDirect;
            nControl2.ChangedAntennaType += NControl2_ChangedAntennaType;
            cControl.ChangedCoordinates += CControl_ChangedCoordinates;
            nControl2.OnNaviBtnsClick += NControl_OnNaviBtnsClick;

            CurrentTheme = Theme.Blue;

            translations = new Translations(Properties.Resources.Translations, MyGrid.Children);

            ucState.SetTranslation(StateNAVControl.Language.RU);
            InitDefaultBPSS();
        }


        private void CControl_ChangedCoordinates(object sender, CoordinatesModel e)
        {
            iControl2.ImageValue.Angle = e.Heading;
            ChangedCoordinates?.Invoke(this, e);
        }

        private void NControl2_ChangedAntennaType(object sender, AntennaMode e)
        {
            ChangedAntennaType?.Invoke(this, e);
        }

        private void NControl2_ChangedAntennaDirect(object sender, int e)
        {
            ChangedAntennaDirect?.Invoke(this, e);
        }

        private void NControl_OnNaviBtnsClick(bool isCheked, Navi navi, L l)
        {
            ChangedNavigation?.Invoke(this, new NavigationModel() { IsCheked = isCheked, Navi = navi, L = l });
        }


        public AntennaModel Antenna
        {
            get => spoofingViewModel.TempAntenna;
        }

        public CoordinatesModel SpoofParameters
        {
            get => spoofingViewModel.TempCoordinates;
            set
            {
                if (spoofingViewModel.TempCoordinates == value) return;
                spoofingViewModel.TempCoordinates = value;
            }
        }


        //public StateModel1 State1
        //{
        //    get => spoofingViewModel.TempState1;
        //    set
        //    {
        //        if (spoofingViewModel.TempState1 == value) return;
        //        spoofingViewModel.TempState1 = value;
        //    }
        //}

        public StateModel1 State1
        {
            get => iControl2.State1;
            set
            {
                if (iControl2.State1 == value) return;
                iControl2.State1 = value;
            }
        }


        public StateModel2 State2
        {
            get => spoofingViewModel.TempState2;
            set
            {
                if (spoofingViewModel.TempState2 == value) return;
                spoofingViewModel.TempState2 = value;
            }
        }


        public ImageModel ImageAngle
        {
            get => spoofingViewModel.TempImage;
            set
            {
                if (spoofingViewModel.TempImage == value) return;
                spoofingViewModel.TempImage = value;
            }
        }


        public void SetAntennaMode(AntennaMode AntennaMode)
        {
            nControl2.SetAntennaMode(AntennaMode);
        }

        public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register("Orientation", typeof(OrientationEnum), typeof(SpoofingControlView2),
       new FrameworkPropertyMetadata(OrientationEnum.Horizontal, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public OrientationEnum Orientation
        {
            get { return (OrientationEnum)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public void ChangeLanguage(Languages language)
        {
            translations.CurrentLanguage = language;

            cControl.translations.CurrentLanguage = language;

            iControl2.translations.CurrentLanguage = language;

            nControl2.ChangeLanguage(language);

            ucState.SetTranslation(LanguageConverter(language));
            ucState.ListStateModel = new List<StateModel>(ucState.ListStateModel);
        }

        private StateNAVControl.Language LanguageConverter(Languages language)
        {
            switch (language)
            {
                case Languages.RU: return StateNAVControl.Language.RU;
                case Languages.EN: return StateNAVControl.Language.EN;
                case Languages.AZ: return StateNAVControl.Language.AZ;

                default: return StateNAVControl.Language.RU;
            }
        }

        private Theme _CurrentTheme;
        public Theme CurrentTheme
        {
            get { return _CurrentTheme; }
            set
            {
                _CurrentTheme = value;
                SetTheme(value);
            }
        }

        private void SetTheme(Theme CurrentTheme)
        {
            ThemeSwither.SetTheme(CurrentTheme, this);
            cControl.CurrentTheme = CurrentTheme;
            iControl2.CurrentTheme = CurrentTheme;
            nControl2.CurrentTheme = CurrentTheme;
        }

        private void tbNaviJam_Click(object sender, RoutedEventArgs e)
        {
            nControl2.IsEnabledButtons = !tbNaviJam.IsChecked.Value;

            ChangedMainToggleButtons?.Invoke(this, new MainToggleButtonsModel() 
            { 
                IsCheked = tbNaviJam.IsChecked.Value, 
                SpoofAndNavi = SpoofOrNavi.NaviJamming,
                PowerLevel = (byte)nControl2.powerLevel.Value
            });
        }

        private void tbSpoof_Click(object sender, RoutedEventArgs e)
        {
            cControl.IsEnabledButtons = !tbSpoof.IsChecked.Value;

            ChangedMainToggleButtons?.Invoke(this, new MainToggleButtonsModel() 
            { 
                IsCheked = tbSpoof.IsChecked.Value, 
                SpoofAndNavi = SpoofOrNavi.Spoofing, 
                PowerLevel = (byte)cControl.powerLevel.Value
            });

        }


        private void InitDefaultBPSS()
        {
            List<StateModel> list = GenerateDefaultBPSSList();
            ucState.ListStateModel = list;
        }
        private List<StateModel> GenerateDefaultBPSSList()
        {
            List<StateModel> list = new List<StateModel>()
            {
                new StateModel
                {
                    Id = -2,
                    Type = StateNAVControl.Type.NAV,
                    Temperature = -102,
                    Amperage = -2,
                    Snt = StateNAVControl.Led.Gray,
                    Rad = StateNAVControl.Led.Gray,
                    Pow = StateNAVControl.Led.Gray
                },
                new StateModel
                {
                    Id = -2,
                    Type = StateNAVControl.Type.SPUF,
                    Temperature = -102,
                    Amperage = -2,
                    Snt = StateNAVControl.Led.Gray,
                    Rad = StateNAVControl.Led.Gray,
                    Pow = StateNAVControl.Led.Gray
                }
            };
            return list;
        }

        public void SetBPSSParams(IEnumerable<StateModel> Params, StateNAVControl.Type Type = StateNAVControl.Type.Empty)
        {
            List<StateModel> list = GenerateDefaultBPSSList();

            switch (Type)
            {
                case StateNAVControl.Type.NAV:

                    if (Params.Count() > 0)
                    {
                        list[0] = new StateModel
                        {
                            Id = -2,
                            Type = StateNAVControl.Type.NAV,
                            Temperature = Params.ElementAt(0).Temperature,
                            Amperage = Params.ElementAt(0).Amperage,
                            Snt = Params.ElementAt(0).Snt,
                            Rad = Params.ElementAt(0).Rad,
                            Pow = Params.ElementAt(0).Pow
                        };
                    }

                    break;

                case StateNAVControl.Type.SPUF:

                    if (Params.Count() > 0)
                    {
                        list[1] = new StateModel
                        {
                            Id = -2,
                            Type = StateNAVControl.Type.SPUF,
                            Temperature = Params.ElementAt(0).Temperature,
                            Amperage = Params.ElementAt(0).Amperage,
                            Snt = Params.ElementAt(0).Snt,
                            Rad = Params.ElementAt(0).Rad,
                            Pow = Params.ElementAt(0).Pow
                        };
                    }

                    break;

                case StateNAVControl.Type.Empty:

                    if (Params.Count() > 1)
                    {
                        list[0] = new StateModel
                        {
                            Id = -2,
                            Type = StateNAVControl.Type.NAV,
                            Temperature = Params.ElementAt(0).Temperature,
                            Amperage = Params.ElementAt(0).Amperage,
                            Snt = Params.ElementAt(0).Snt,
                            Rad = Params.ElementAt(0).Rad,
                            Pow = Params.ElementAt(0).Pow
                        };

                        list[1] = new StateModel
                        {
                            Id = -2,
                            Type = StateNAVControl.Type.SPUF,
                            Temperature = Params.ElementAt(1).Temperature,
                            Amperage = Params.ElementAt(1).Amperage,
                            Snt = Params.ElementAt(1).Snt,
                            Rad = Params.ElementAt(1).Rad,
                            Pow = Params.ElementAt(1).Pow
                        };
                    }

                    break;
            }

            DispatchIfNecessary(() =>
            {
                ucState.ListStateModel = list;
            });
        }


        public void NavigationToggleButtonOff()
        {
            DispatchIfNecessary(() =>
            {
                tbNaviJam.IsChecked = false;
                nControl2.IsEnabledButtons = !tbNaviJam.IsChecked.Value;
            });
        }

        public void SpoofingToggleButtonOff()
        {
            DispatchIfNecessary(() =>
            {
                tbSpoof.IsChecked = false;
            });
        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }
    }
}
