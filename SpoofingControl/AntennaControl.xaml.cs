﻿using pTranslations;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace SpoofingControl
{
    /// <summary>
    /// Логика взаимодействия для AntennaControl.xaml
    /// </summary>
    public partial class AntennaControl : UserControl
    {
        //AntennaViewModel antennaViewModel = new AntennaViewModel();

        private Theme _CurrentTheme;
        public Theme CurrentTheme
        {
            get { return _CurrentTheme; }
            set
            {
                _CurrentTheme = value;
                SetTheme(value);
            }
        }
        private void SetTheme(Theme CurrentTheme)
        {
            ThemeSwither.SetTheme(CurrentTheme, this);
        }

        internal event EventHandler<int> ChangedAntennaDirect;
        internal event EventHandler<AntennaMode> ChangedAntennaType;

        public readonly Translations translations;

        public AntennaControl()
        {
            InitializeComponent();
            translations = new Translations(Properties.Resources.TranslationAntenna, MainGrid.Children);
        }


        private void btnType_Clicked(object sender, RoutedEventArgs e)
        {
            var button = (ToggleButton)sender;
            if ((bool)button.IsChecked)
            {
                switch (button.Name)
                {
                    case "btnLog":
                        btnLog.IsChecked = true;
                        btnOmni.IsChecked = false;
                        Antenna.Mode = AntennaMode.Log;
                        ChangedAntennaType?.Invoke(this, AntennaMode.Log);
                        break;
                    default:
                        btnLog.IsChecked = false;
                        btnOmni.IsChecked = true;
                        Antenna.Mode = AntennaMode.OMNI;
                        ChangedAntennaType?.Invoke(this, AntennaMode.OMNI);
                        break;
                }
            }
            else { button.IsChecked = true; }
        }


        private void IntegerUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var direct = Convert.ToInt32(e.NewValue);
            ChangedAntennaDirect?.Invoke(this, direct);
        }



        public static readonly DependencyProperty AntennaProperty = DependencyProperty.Register("Antenna", typeof(AntennaModel), typeof(AntennaControl), 
            new FrameworkPropertyMetadata(new AntennaModel(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public AntennaModel Antenna
        {
            get { return (AntennaModel)GetValue(AntennaProperty); }
            set { SetValue(AntennaProperty, value); }
        }

        internal void SetAntennaMode(AntennaMode AntennaMode)
        {
            switch (AntennaMode)
            {
                case AntennaMode.OMNI:
                    btnOmni.IsChecked = true;
                    btnType_Clicked(btnOmni, null);
                    break;
                case AntennaMode.Log:
                    btnLog.IsChecked = true;
                    btnType_Clicked(btnLog, null);
                    break;
            }
        }
    }
}
