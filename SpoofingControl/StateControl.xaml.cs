﻿using pTranslations;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SpoofingControl
{
    /// <summary>
    /// Логика взаимодействия для StateControl.xaml
    /// </summary>
    public partial class StateControl : UserControl
    {
        private Theme _CurrentTheme;
        public Theme CurrentTheme
        {
            get { return _CurrentTheme; }
            set
            {
                _CurrentTheme = value;
                SetTheme(value);
            }
        }
        private void SetTheme(Theme CurrentTheme)
        {
            ThemeSwither.SetTheme(CurrentTheme, this);
        }

        public readonly Translations translations;

        public StateControl()
        {
            InitializeComponent();

            translations = new Translations(Properties.Resources.TranslationsState, MainGrid.Children);
        }

        public static readonly DependencyProperty StateProperty1 = DependencyProperty.Register("State1", typeof(StateModel1), typeof(StateControl),
            new FrameworkPropertyMetadata(new StateModel1 { Latitude = 0, Longitude = 0, Time = DateTime.Now}, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public StateModel1 State1
        {
            get { return (StateModel1)GetValue(StateProperty1); }
            set { SetValue(StateProperty1, value); }
        }


        public static readonly DependencyProperty StateProperty2 = DependencyProperty.Register("State2", typeof(StateModel2), typeof(StateControl),
          new FrameworkPropertyMetadata(new StateModel2 { Temperature = 0, Current = 0, Snt = 0, Rad = 0, Pow = 0 }, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public StateModel2 State2
        {
            get { return (StateModel2)GetValue(StateProperty2); }
            set { SetValue(StateProperty2, value); }
        }
    }
}
