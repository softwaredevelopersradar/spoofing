﻿using Arction.Wpf.SemibindableCharting.Axes;
using pTranslations;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SpoofingControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ImageControl : UserControl
    {
        private Theme _CurrentTheme;
        public Theme CurrentTheme
        {
            get { return _CurrentTheme; }
            set
            {
                _CurrentTheme = value;
                SetTheme(value);
            }
        }
        private void SetTheme(Theme CurrentTheme)
        {
            ThemeSwither.SetTheme(CurrentTheme, this);
        }

        public readonly Translations translations;

        public ImageControl()
        {
            string deploymentKey = "lgCAABW2ij+vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD+BCRGnn7c6dwaDiJovCk5g5nFwvJ+G60VSdCrAJ+jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq/B0dVcthh7ezOUzf1uXfOcEJ377/4rwUTR0VbNTCK601EN6/ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM+Q5vztCEz5k+Luaxs+S+OQD3ELg8+y7a/Dv0OhSQkqMDrR/o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry/tAsPPY26Ff3PDl1ItpFWZCzNS/xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi+VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq+F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9+B0YtxFPNBQs=";
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
            InitializeComponent();
            polarChart.ViewPolar.Axes[0].AutoFormatLabels = false;
            polarChart.ViewPolar.Axes[0].SupplyCustomAngleString += AngleAsString;

            translations = new Translations(Properties.Resources.TranslationsImage, MainGrid.Children);
        }

        /// <summary>
        /// Замена градусов на стороны света (на круге)
        /// </summary>
        private void AngleAsString(object sender, SupplyCustomAngleStringEventArgs args)
        {
            int degrees = (int)Math.Round(180f * args.Angle / Math.PI);
            if (degrees == 0)
                args.AngleAsString = "N";

            else if (degrees == 180)
                args.AngleAsString = "S";

            else if (degrees == 90)
                args.AngleAsString = "E";

            else if (degrees == 270)
                args.AngleAsString = "W";
        }


        private void G_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (G.ActualWidth > G.ActualHeight)
            {
                var a = Math.Abs(G.ActualWidth - G.ActualHeight);
                pathRS.Margin = new Thickness(a / 2d, 0, a / 2d, 0);
            }
            if (G.ActualWidth == G.ActualHeight)
            {
                var a = (G.ActualWidth - G.ActualHeight);
                pathRS.Margin = new Thickness(0, 0, 0, 0);
            }
            if (G.ActualWidth < G.ActualHeight)
            {
                var a = Math.Abs(G.ActualHeight - G.ActualWidth);
                pathRS.Margin = new Thickness(0, a / 2d, 0, a / 2d);
            }

            var minSize = Math.Min(G.ActualHeight, G.ActualWidth);

            //double coeff = Math.Round((minSize / 1000d), 1);

            //var scale = ((minSize * (1.0 - coeff)) * 0.6) / 200d;

            double coeff = Math.Round((minSize / 100d), 1);

            var scale = ((minSize * (10 - coeff) /10d) * 0.6) / 200d;

            ST.ScaleX = scale;
            ST.ScaleY = scale;

            //ST.ScaleX = 0.8 * (ActualWidth / 450d);
            //ST.ScaleY = 0.8 * (ActualHeight / 450d);
        }


        public static readonly DependencyProperty ImageProperty = DependencyProperty.Register("ImageValue", typeof(ImageModel), typeof(ImageControl),
            new FrameworkPropertyMetadata(new ImageModel(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public ImageModel ImageValue
        {
            get { return (ImageModel)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }
    }
}
