﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SpoofingControl
{
    public class VisibilityView : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((View)value)
            {
                case View.Normal:
                    return Visibility.Collapsed;
                case View.Pro:
                    return Visibility.Visible;
                default:
                    return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((Visibility)value)
            {
                case Visibility.Collapsed:
                    return View.Normal;

                case Visibility.Visible:
                    return View.Pro;

                case Visibility.Hidden:
                    return View.Normal;

                default:
                    return null;
            }
        }
    }
}
