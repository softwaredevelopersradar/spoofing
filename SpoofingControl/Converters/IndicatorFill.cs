﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media;

namespace SpoofingControl
{
    class IndicatorFill : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            LinearGradientBrush FillPath;
            var gradientStops = new GradientStopCollection();
        
            switch ((byte)value)
            {
                case 1:
                    //green
                    gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF38FF38"), 0));
                    gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF04FF04"), 1));
                    break;
                    

                default:
                    //red
                    gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFFF3838"), 0));
                    gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFFD0404"), 1));
                    break;
            }

            FillPath = new LinearGradientBrush(gradientStops);
            FillPath.EndPoint = new Point(0, 1);
            FillPath.StartPoint = new Point(0, 0);

            return FillPath;
        }

        //yellow
        //gradientStops.Add(new GradientStop((Color) ColorConverter.ConvertFromString("#FFDFD991"), 0));
        //gradientStops.Add(new GradientStop((Color) ColorConverter.ConvertFromString("#FFFD9804"), 1));

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var FillPath = (LinearGradientBrush)value;
            if (FillPath.GradientStops[0].Color == (Color)ColorConverter.ConvertFromString("#FF38FF38"))
            {
                return 1;
            }
            else
            { return 0; }
        }
    }
}
