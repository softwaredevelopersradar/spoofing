﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SpoofingControl
{
    public class CorrectAngle : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value - 90;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value + 90;
        }
    }
}
