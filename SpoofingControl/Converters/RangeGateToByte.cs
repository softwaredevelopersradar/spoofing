﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SpoofingControl
{
    public class RangeGateToByte : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var rangeGate = (byte)value;

            if (CoordinatesControl.rangeGateList.Count < rangeGate)
                return "Error";

            return CoordinatesControl.rangeGateList[rangeGate - 1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var range = (string)value;

            return CoordinatesControl.rangeGateList.IndexOf(range) + 1;
        }
    }
}
