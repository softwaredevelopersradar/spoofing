﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Controls;


namespace SpoofingControl
{
    public class DockPanelOrientation : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((OrientationEnum)value)
            {
                case OrientationEnum.Horizontal:
                    return Dock.Left;


                default:
                    return Dock.Top;
            }
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((Dock)value)
            {
                case  Dock.Left:
                    return OrientationEnum.Horizontal;
                default:
                    return OrientationEnum.Vertical;
            }
        }
    }
}
