﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SpoofingControl
{
    public class SpoofingViewModel : INotifyPropertyChanged
    {
        public SpoofingViewModel()
        {
            SetDefaultProperties();
        }

        //public event EventHandler<int> ChangedAntennaDirect;
        //public event EventHandler<AntennaMode> ChangedAntennaType;

        private void SetDefaultProperties()
        {
            TempAntenna = new AntennaModel() { Direct = 0, Mode = AntennaMode.OMNI };
            TempCoordinates = new CoordinatesModel() { Altitude = 0, Latitude = 0, Longitude = 0, Heading = 0, Speed = 0, RangeGate = 1 };
            TempState1 = new StateModel1() { Latitude = 0, Longitude = 0, Time = DateTime.Now};
            TempState2 = new StateModel2() { Temperature = 0, Current = 0, Snt = 0, Rad = 0, Pow = 0 };
            TempImage = new ImageModel() { Angle = 0};
        }

        private AntennaModel _tempAntenna;

        public AntennaModel TempAntenna
        {
            get { return _tempAntenna; }
            set
            {
                if (_tempAntenna == value) return;

                //if (_tempAntenna != null && _tempAntenna.Direct != value.Direct)
                //    ChangedAntennaDirect?.Invoke(this, value.Direct);
                //if (_tempAntenna != null && _tempAntenna.Mode != value.Mode)
                //    ChangedAntennaType?.Invoke(this, value.Mode);
                _tempAntenna = value;
                OnPropertyChanged();
            }
        }


        private CoordinatesModel _tempCoordinates;

        public CoordinatesModel TempCoordinates
        {
            get { return _tempCoordinates; }
            set
            {
                if (_tempCoordinates == value) return;

                _tempCoordinates = value;
                OnPropertyChanged();
            }
        }



        private StateModel1 _tempState1;

        public StateModel1 TempState1
        {
            get { return _tempState1; }
            set
            {
                if (_tempState1 == value) return;

                _tempState1 = value;
                OnPropertyChanged();
            }
        }


        private StateModel2 _tempState2;

        public StateModel2 TempState2
        {
            get { return _tempState2; }
            set
            {
                if (_tempState2 == value) return;

                _tempState2 = value;
                OnPropertyChanged();
            }
        }


        private ImageModel _tempImage;

        public ImageModel TempImage
        {
            get { return _tempImage; }
            set
            {
                if (_tempImage == value) return;

                _tempImage = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
