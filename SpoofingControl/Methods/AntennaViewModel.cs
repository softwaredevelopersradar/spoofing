﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace SpoofingControl
{
    public class AntennaViewModel : DependencyObject, INotifyPropertyChanged
    {
        public AntennaViewModel()
        { 
            //_antennaSelected.PropertyChanged += _antennaSelected_PropertyChanged; 
        }

        //private void _antennaSelected_PropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    var r = e.PropertyName;
        //}

        private AntennaModel _antennaSelected = new AntennaModel();
        public AntennaModel AntennaSelected
        {
            get
            {
                return _antennaSelected;
            }
            set
            {
                _antennaSelected = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}